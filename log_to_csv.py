#! /usr/bin/env python3
import csv
import re
import sys
from pathlib import Path
from typing import List, Dict


def records_in_line(line: str) -> List[Dict[str, str]]:
    rec_strings = line.split("type=sensor;")[1:]
    return [parse_record(rec) for rec in rec_strings]


def parse_record(rec: str) -> Dict[str, str]:
    key_vals = re.findall(r"(\w+)=([^=]+);", rec)
    return dict(key_vals)


def main(argv):
    input_dir = Path(argv[1])
    output_dir = Path(argv[2])
    input_files = list(input_dir.glob("*.log"))
    last_date_str = sorted([inf.stem for inf in input_files])[-1]
    log_entries = []
    for in_file in input_files:
        for line in in_file.open():
            log_entries.extend(records_in_line(line))

    all_keys = set()
    for entry in log_entries:
        all_keys.update(entry.keys())

    output_file = (output_dir / last_date_str).with_suffix(".csv")
    with output_file.open("w", newline="") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=sorted(list(all_keys)), dialect="unix")
        writer.writeheader()
        writer.writerows(log_entries)


if __name__ == "__main__":
    main(sys.argv)
