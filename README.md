# Sensor sensibility

## Convert sensor log entries to a CSV file

1. python3 -m virtualenv venv
2. source venv/bin/activate
3. ./log_to_csv.py data/Logfiler data/csv

The output file in data/csv will be named after the last date found in data/Logfiler.

## Generate plots

1. python3 -m pip install -r requirements.txt
2. streamlit run plots.py

A browser window should now open and show the plots at [http://localhost:8501](http://localhost:8501).
