import csv
from datetime import datetime
from pathlib import Path
from typing import Dict, Any, Optional, Iterable, List

import streamlit as st
from bokeh import palettes
from bokeh.models import DatetimeTickFormatter
from bokeh.models.ranges import Range1d
from bokeh.plotting import figure

WIDTH = 1000


def load_csv():
    last_file = sorted(list(Path("data/csv").glob("*.csv")))[-1]
    with last_file.open(newline="") as csv_file:
        reader = csv.DictReader(csv_file, dialect="unix")
        return list(reader)


def optional_int(value: str) -> Optional[int]:
    return int(value) if value else None


def interpret_types(record: Dict[str, str]) -> Dict[str, Any]:
    factories = {
        "age": optional_int,
        "humidity": optional_int,
        "id": optional_int,
        "model": str,
        "protocol": str,
        "temperature": float,
        "time": datetime.fromisoformat
    }
    return {k: factories[k](v) for k, v in record.items()}


def group_by(items: Iterable, key_fn, val_transform=lambda v: v) -> dict:
    """Group items based on a key into a dictionary of lists of items that share the same key.

    Optionally transform the values before insertion.
    """
    ret = {}
    for it in items:
        k = key_fn(it)
        ret[k] = ret.get(k, []) + [val_transform(it)]
    return ret


def is_valid(rec: dict) -> bool:
    return rec["time"] >= datetime(2022, 1, 1, 0, 0, 0)


st.set_page_config(layout="wide")
st.title("Sense or sensibility")

raw_records = load_csv()

all_records = [interpret_types(rec) for rec in raw_records]

valid_records = [rec for rec in all_records if is_valid(rec)]

humidity_recs = [rec for rec in valid_records
                 if rec["model"] == "temperaturehumidity"]

all_ids = sorted(set([rec["id"] for rec in valid_records]))
humidity_ids = sorted(set([rec["id"] for rec in humidity_recs]))

num_data_points = {sid: len(points) for sid, points in group_by(valid_records, lambda rec: rec["id"]).items()}


# num_box_columns = 4
# box_columns = st.columns(num_box_columns)
# id_boxes = [sid for i, sid in enumerate(all_ids) if
#             box_columns[i % num_box_columns].checkbox(f"{sid} ({num_data_points[sid]})", value=True)]


def line_plot(records: List[dict], ids: List[int], y_field: str, title: str):
    y_labels = {"temperature": "grader C", "humidity": "percent"}
    temperature_fig = figure(title=title, x_axis_type="datetime", y_axis_label=y_labels[y_field], width=WIDTH,
                             x_range=Range1d(datetime(2021,12,20,0,0,0),datetime(2022,7,10,0,0,0)),
    toolbar_location=None, tools="")
    for count, temp_id in enumerate(ids):
        temp_id_recs = sorted([rec for rec in records if rec["id"] == temp_id], key=lambda r: r["time"])
        datetimes = [rec["time"] for rec in temp_id_recs]
        temp_values = [rec[y_field] for rec in temp_id_recs]
        color = palettes.Category20_20[count % 20]
        temperature_fig.line(datetimes, temp_values, legend_label=str(temp_id), line_width=2, color=color)
    temperature_fig.xaxis.formatter = DatetimeTickFormatter(days=["%Y-%m-%d"])
    st.bokeh_chart(temperature_fig)
    st.markdown('<br />' * 3, unsafe_allow_html=True)
    st.markdown('<div style="page-break-after: always;"></div>', unsafe_allow_html=True)


# line_plot(
#     records=humidity_recs,
#     ids=[hi for hi in humidity_ids if hi in id_boxes],
#     y_field="humidity",
#     title="Fuktighet",
# )
# 
# line_plot(
#     records=valid_records,
#     ids=[hi for hi in all_ids if hi in id_boxes],
#     y_field="temperature",
#     title="Temperatur"
# )
# 
hum_id_groups = [
    # ("Skorsten", [49, 99, 106]),
    ("Källaren", [135]),
    ("Ute", [151]),
    ("Vinden", [167]),
    ("Inne", [183]),
]


st.markdown("## Fuktighet")

for name, id_grp in hum_id_groups:
    line_plot(records=humidity_recs, ids=id_grp, y_field="humidity", title=name)

temp_id_groups = [
                     ("Uthyrningen", [109,]),  # 106, 204, 212 247
                 ] + hum_id_groups

st.markdown('<br />' * 3, unsafe_allow_html=True)

st.markdown("## Temperatur")

for name, id_grp in temp_id_groups:
    line_plot(records=valid_records, ids=id_grp, y_field="temperature", title=name)
    st.markdown('<br />' * 1, unsafe_allow_html=True)
